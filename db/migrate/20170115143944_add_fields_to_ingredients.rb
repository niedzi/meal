class AddFieldsToIngredients < ActiveRecord::Migration[5.0]
  def change
    add_column :ingredients, :fat, :decimal
    add_column :ingredients, :protein, :decimal
    add_column :ingredients, :carbohydrates, :decimal
    add_column :ingredients, :kcal, :decimal
  end
end
