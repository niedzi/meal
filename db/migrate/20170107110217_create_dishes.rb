class CreateDishes < ActiveRecord::Migration[5.0]
  def change
    create_table :dishes do |t|
      t.integer :day_diet_id

      t.timestamps
    end
  end
end
