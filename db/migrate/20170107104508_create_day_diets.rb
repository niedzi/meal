class CreateDayDiets < ActiveRecord::Migration[5.0]
  def change
    create_table :day_diets do |t|
      t.integer :diet_id
      t.string :name

      t.timestamps
    end
  end
end
