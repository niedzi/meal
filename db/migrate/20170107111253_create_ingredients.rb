class CreateIngredients < ActiveRecord::Migration[5.0]
  def change
    create_table :ingredients do |t|
      t.integer :dish_id
      t.integer :product_id
      t.integer :amount

      t.timestamps
    end
  end
end
