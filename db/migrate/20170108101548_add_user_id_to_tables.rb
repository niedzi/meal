class AddUserIdToTables < ActiveRecord::Migration[5.0]
  def change
    add_column :day_diets, :user_id, :integer
    add_column :dishes, :user_id, :integer
    add_column :dishes, :name, :string
  end
end
