class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.decimal :kcal, precision: 6, scale: 2, null: false
      t.decimal :protein, precision: 6, scale: 2, null: false
      t.decimal :fat, precision: 6, scale: 2, null: false
      t.decimal :carbohydrates, precision: 6, scale: 2, null: false

      t.integer :vendor_id
      t.integer :category_id

      t.timestamps
    end
  end
end
