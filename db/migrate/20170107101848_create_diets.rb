class CreateDiets < ActiveRecord::Migration[5.0]
  def change
    create_table :diets do |t|
      t.integer :user_id
      t.string :name
      t.integer :kcal

      t.timestamps
    end
  end
end
