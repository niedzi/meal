Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "home#index"
  # devise_for :users
  devise_for :users, controllers: {
      sessions: 'users/sessions',
      registrations: 'users/registrations',
      confirmations: 'users/confirmations',
      unlocks: 'users/unlocks',
      passwords: 'user/passwords'
  }

  resources :products do
    collection do
      get :search
      post :search_for_dish
    end
  end

  get "/calculators" => "home#calculators", as: :calculators
  get "/informacje" => "home#info", as: :info
  post "/bmi" => "home#bmi", as: :bmi

  resources :day_diets
  resources :diets
  resources :dishes
  resources :ingredients do
    member do
      get :info
    end
  end

end
