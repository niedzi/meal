@clearErrors = () ->
  $(".input-field .error").each ->
    $(@).html("")

@showErrors = (response,object) ->
  Materialize.toast(response.responseJSON.message, 4000, 'red')
  for k,v of response.responseJSON.errors
    $("##{object}_#{k}").parents(".input-field").children(".error").html(v)

$ ->
  if $('#permission_error_handle').length == 1
    Materialize.toast($('#permission_error_handle').data('message'), 4000, 'red')

  $('.slide-menu').sideNav()
  $('.modal').modal();
  $('.parallax').parallax();

#  $(document).on
#    click: (e) ->
#      e.preventDefault()
#      $(@).parents('.modal').modal('close')
#  ,".modal-close"

  $("#js-session-devise-form").ajaxForm
    dataType: 'json'
    type: 'POST'
    beforeSend: () ->
      $("form .actions.input-field").addClass("hide")
      $("#shared-links").addClass("hide")
      $("#preloader").removeClass("hide")
    success: (response) ->
      Materialize.toast(response.message, 4000, 'green')
      $("#preloader").removeClass("hide")
      window.location.href = response.location
    error: (response) ->
      Materialize.toast(response.responseJSON.message, 4000, 'red')
      $("form .actions.input-field").removeClass("hide")
      $("#shared-links").removeClass("hide")
      $("#preloader").addClass("hide")

  $("#js-register-devise-form").ajaxForm
    dataType: 'json'
    type: 'POST'
    beforeSend: () ->
      clearErrors()
      $("form .actions.input-field").addClass("hide")
      $("#shared-links").addClass("hide")
      $("#preloader").removeClass("hide")
    success: (response) ->
      Materialize.toast(response.message, 4000, 'green')
      $("#preloader").removeClass("hide")
      window.location.href = response.location
    error: (response) ->
      showErrors(response,"user")
      $("form .actions.input-field").removeClass("hide")
      $("#shared-links").removeClass("hide")
      $("#preloader").addClass("hide")

  console.log 'ready'