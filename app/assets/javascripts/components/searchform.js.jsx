var SearchForm = React.createClass({
    handleSearch: function() {
        var name_search = ReactDOM.findDOMNode(this.refs.name_search).value;
        var protein_gte = ReactDOM.findDOMNode(this.refs.protein_gte).value;
        var fat_gte = ReactDOM.findDOMNode(this.refs.fat_gte).value;
        var carbohydrates_gte = ReactDOM.findDOMNode(this.refs.carbohydrates_gte).value;
        var self = this;
        $.ajax({
            url: '/products/search',
            data: { query: {name_search: name_search, protein_gte: protein_gte, fat_gte: fat_gte, carbohydrates_gte: carbohydrates_gte} },
            success: function(response) {
                self.props.handleSearch(response);
            },
            error: function(xhr, status, error) {
                alert('Search error: ', status, xhr, error);
            }
        });
    },
    render: function() {
        return(
            <div>
                <div className="col s12 l3">
                    <label>Nazwa</label>
                    <input onChange={this.handleSearch}
                           type="text"
                           placeholder="Wpisz czego szukasz..."
                           ref="name_search" />
                </div>
                <div className="col s12 l3">
                    <label>Białko od</label>
                    <input onChange={this.handleSearch}
                        type="number"
                        min="0"
                        ref="protein_gte" />
                </div>
                <div className="col s12 l3">
                    <label>Tłuszcz od</label>
                    <input onChange={this.handleSearch}
                           type="number"
                           min="0"
                           ref="fat_gte" />
                </div>
                <div className="col s12 l3">
                    <label>Węglowodany od</label>
                    <input onChange={this.handleSearch}
                           type="number"
                           min="0"
                           ref="carbohydrates_gte" />
                </div>
            </div>
        )
    }
});