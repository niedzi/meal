var ProductsTable = React.createClass({
   render: function(){
       var products = [];
       this.props.products.forEach(function (product) {
           products.push(<Product product={product} key={'product'+product.id}/>);
       }.bind(this));
       return(
          <table className="striped highlight centered">
              <thead>
                <tr>
                    <th>Nazwa</th>
                    <th>Kcal</th>
                    <th>Białko</th>
                    <th>Tłuszcz</th>
                    <th>Węglowodany</th>
                </tr>
              </thead>
              <tbody>
                {products}
              </tbody>
          </table>
       )
   }
});
