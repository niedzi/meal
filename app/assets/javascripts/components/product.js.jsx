var Product = React.createClass({
    popTypes:{
        name: React.PropTypes.string,
        kcal: React.PropTypes.string,
        protein: React.PropTypes.string,
        fat: React.PropTypes.string,
        carbohydrates: React.PropTypes.string
    },
   render: function(){
       var product = this.props.product;
       return(
           <tr>
               <td>{product.name}</td>
               <td>{product.kcal}</td>
               <td>{product.protein}</td>
               <td>{product.fat}</td>
               <td>{product.carbohydrates}</td>
           </tr>
       )
   }
});
