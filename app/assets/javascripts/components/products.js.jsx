var Products = React.createClass({
    getInitialState: function () {
        return { products: [] }
    },
    componentDidMount: function () {
        this.getDataFromApi();
    },
    getDataFromApi: function () {
      var self = this;
      $.ajax({
          url: '/products/search',
          success: function (response) {
              self.setState({ products: response })
          },
          error: function (response) {
              alert('Błąd łączenia z API', response)
          }
      });
    },
    handleSearch: function (products) {
        this.setState({products: products});
    },
   render: function(){
       return(
           <div className="row">
               <div className="container">
                   <h1>Produkty</h1>
                   <SearchForm handleSearch={this.handleSearch} />
                   <ProductsTable products={this.state.products} />
               </div>
           </div>
       )
   }
});
