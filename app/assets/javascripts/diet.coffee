@jsNewDiet = () ->
  $("#js-new-diet").ajaxForm
    dataType: 'json'
    type: 'POST'
    beforeSend: () ->
      clearErrors()
      $("form .actions.input-field").addClass('disabled')
    success: (response) ->
      if response.location.length == 0
        Materialize.toast(response.message, 4000, 'green')
        $("#diet-list").append(response.diet_item)
        $("#js-new-modal").html('').modal('close')
      else
        window.location.href = response.location
    error: (response) ->
      showErrors(response,"diet")
      $("form .actions.input-field").removeClass("disabled")

$ ->
  $(document).on
    click: (e) ->
      e.preventDefault()
      $("#js-destroy-modal .js-destroy-diet").data('id',$(@).data('id'))
      $("#js-destroy-modal").modal('open')
  ,".js-destroy-modal"

  $(document).on
    click: (e) ->
      e.preventDefault()
      id = $(@).data('id')
      tmp_this = $("tr#diet#{id}")
      $.ajax
        dataType: 'json'
        type: 'DELETE'
        url: "/diets/#{id}"
        beforeSend: () ->
          $("#js-destroy-modal .js-destroy-diet").addClass('disabled')
          $("#js-destroy-modal .modal-close").addClass('disabled')
        success: (response) ->
          tmp_this.remove()
          $("#js-destroy-modal .js-destroy-diet").data('id','').removeClass('disabled')
          $("#js-destroy-modal .modal-close").removeClass('disabled')
          $("#js-destroy-modal").modal('close')
          Materialize.toast(response.message, 4000, 'green')
        error: (response) ->
          $("#js-destroy-modal .js-destroy-diet").removeClass('disabled')
          $("#js-destroy-modal .modal-close").removeClass('disabled')
          Materialize.toast(response.message, 4000, 'red')
  ,".js-destroy-diet"

  $(document).on
    click: (e) ->
      e.preventDefault()
      $.ajax
        dataType: 'html'
        type: 'GET'
        url: $(@).attr('href')
        success: (response) ->
          $("#js-new-modal").html(response).modal('open')
          jsNewDiet()
        error: (response) ->
          Materialize.toast('Wystąpił błąd', 4000, 'red')
  ,".js-new-diet"