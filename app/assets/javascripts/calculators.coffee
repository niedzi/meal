$ ->
  $(document).on
    click: (e) ->
#      e.preventDefault()
      $.ajax
        dataType: 'json'
        type: 'POSt'
        url: "/bmi"
        data: {weight: $("#bmi_weight").val(), height: $("#bmi_height").val()}
        beforeSend: ->
          $(".collection-item").removeClass('lime red active')
        success: (response) ->
          $("#bmi-val").html("Twoje BMI: #{response.bmi}")
          $(".bmi-result .#{response.active}").addClass("active #{response.color}")
        error: (response) ->
          Materialize.toast('Żadna z wartośnie nie może wynosić zero!', 4000, 'red')
  ,"#js-bmi"