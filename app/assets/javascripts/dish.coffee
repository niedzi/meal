@jsNewDish = () ->
  $("#js-new-dish").ajaxForm
    dataType: 'json'
    type: 'POST'
    beforeSend: () ->
      clearErrors()
      console.log 'test'
      $("form .actions.input-field").addClass("disabled")
    success: (response) ->
      if response.location.length == 0
        Materialize.toast(response.message, 4000, 'green')
        $("#dish-list").append(response.day_diet_item)
        $("#js-new-modal").html('').modal('close')
      else
        window.location.href = response.location
    error: (response) ->
      showErrors(response,"day_diet")
      $("form .actions.input-field").removeClass("disabled")

$ ->
  $(document).on
    click: (e) ->
      e.preventDefault()
      $("#js-destroy-modal .js-destroy-dish").data('id',$(@).data('id'))
      $("#js-destroy-modal").modal('open')
  ,".js-destroy-modal"

  $(document).on
    click: (e) ->
      e.preventDefault()
      id = $(@).data('id')
      tmp_this = $("#dish#{id}")
      $.ajax
        dataType: 'json'
        type: 'DELETE'
        url: "/dishes/#{id}"
        beforeSend: () ->
          $("#js-destroy-modal .js-destroy-dish").addClass('disabled')
          $("#js-destroy-modal .modal-close").addClass('disabled')
        success: (response) ->
          tmp_this.remove()
          $("#js-destroy-modal .js-destroy-dish").data('id','').removeClass('disabled')
          $("#js-destroy-modal .modal-close").removeClass('disabled')
          $("#js-destroy-modal").modal('close')
          Materialize.toast(response.message, 4000, 'green')
        error: (response) ->
          $("#js-destroy-modal .js-destroy-dish").removeClass('disabled')
          $("#js-destroy-modal .modal-close").removeClass('disabled')
          Materialize.toast(response.message, 4000, 'red')
  ,".js-destroy-dish"

  $(document).on
    click: (e) ->
      e.preventDefault()
      $.ajax
        dataType: 'html'
        type: 'GET'
        url: $(@).attr('href')
        success: (response) ->
          $("#js-new-modal").html(response)
          $("form #day_diet_id").val($("#day-diet-id").data('id'))
          jsNewDish()
          $("#js-new-modal").modal('open')
        error: (response) ->
          Materialize.toast('Wystąpił błąd', 4000, 'red')
  ,".js-new-dish"

  $('#dish').droppable({
    accept: '.product',
    hoverClass: "hoverable",
    drop: (event, ui) ->
      drop_this = $(this)
      $('#product-modal').modal('open')
      dish_id = $('#dish-content').data('id')
      $('#add-dish').click (e) ->
        e.preventDefault()
        $.ajax
          dataType: 'json'
          type: 'POST'
          url: "/ingredients"
          data: {ingredient: {dish_id: dish_id, product_id: ui.draggable.data('id'), amount: $('#product-modal #amount').val()}}
          success: (response) ->
            Materialize.toast(response.message, 4000, 'green')
            $("#dish_totals").html(response.totals)
            if response.check == 'true' or response.check == true
              Materialize.toast("Przekroczyłeś wyznaczoną ilość kilokalorii", 4000, 'red')
            drop_this.append(response.product)
            $('#product-modal').modal('close')
            $('#product-modal #amount').val('')
          error: (response) ->
            console.log response.message
        $('#add-dish').unbind('click')
  })

  $('.product').draggable({
    revert: "invalid",
    cursor: 'move',
    containment: 'main',
    helper: 'clone',
    appendTo: '#dish',
    start: (event, ui) ->
      isDraggingMedia = true
    stop: (event, ui) ->
      isDraggingMedia = true
  })

  $(document).on
    click: (e) ->
      $.ajax
        dataType: 'json'
        type: 'GET'
        url: "/ingredients/#{$(@).data('id')}/info"
        success: (response) ->
          $("#ingredient_info").html(response.info)
          $("#delete-dish").data('id',response.id)
          $("#update-dish").data('id',response.id)

          $('#ingredient-modal').modal('open')
        error: (response) ->
          Materialize.toast(response.message, 4000, 'red')
  ,".product-onlist"

  $(document).on
    click: (e) ->
      $.ajax
        dataType: 'json'
        type: 'DELETE'
        url: "/ingredients/#{$(@).data('id')}"
        success: (response) ->
          $("#dish_totals").html(response.totals)
          $("#ingredient#{response.id}").remove()
          $('#ingredient-modal').modal('close')
          Materialize.toast(response.message, 4000, 'green')
        error: (response) ->
          Materialize.toast(response.message, 4000, 'red')
  ,"#delete-dish"

  $(document).on
    click: (e) ->
      $.ajax
        dataType: 'json'
        type: 'PATCH'
        url: "/ingredients/#{$(@).data('id')}"
        data: {ingredient: {amount: $("#amount_update").val() }}
        success: (response) ->
          if response.check == 'true' or response.check == true
            Materialize.toast("Przekroczyłeś wyznaczoną ilość kilokalorii", 4000, 'red')
          $("#dish_totals").html(response.totals)
          $('#ingredient-modal').modal('close')
          Materialize.toast(response.message, 4000, 'green')
        error: (response) ->
          Materialize.toast(response.message, 4000, 'red')
  ,"#update-dish"

  $(document).on
    keyup: (e) ->
      if $(@).val().length >= 3
        $.ajax
          dataType: 'json'
          type: 'POST'
          url: '/products/search_for_dish'
          data: {query: $(@).val()}
          beforeSend: ->
            $("#search_products").attr('disabled','disabled')
          success: (response) ->
            $(".products_group .group-product-search").html(response.products)
            $("#search_products").removeAttr('disabled').focus()
            $('.product').draggable({
              revert: "invalid",
              cursor: 'move',
              containment: 'main',
              helper: 'clone',
              appendTo: '#dish',
              start: (event, ui) ->
                isDraggingMedia = true
              stop: (event, ui) ->
                isDraggingMedia = true
            })
          error: (response) ->
            $("#search_products").removeAttr('disabled')
            console.log response
  ,'#search_products'