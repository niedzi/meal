class DishesController < ApplicationController

  load_and_authorize_resource

  def new
    @new_dish = Dish.new
    render layout: false
  end

  def create
    day_diet = DayDiet.find(params[:day_diet_id])
    @new_dish = day_diet.dishes.new(dish_params)
    @new_dish.user_id = current_user.id
    current_user.dishes.count == 0 ? redirect = day_diet_path(day_diet) : redirect = ""
    respond_to do |format|
      if @new_dish.save
        format.json { render json: {message: t("activerecord.dish.dish_created"), day_diet_item: render_to_string(partial: 'dish_item', locals: {dish: @new_dish}).html_safe, location: redirect}, status: :ok }
      else
        format.json { render json: {message: t('errors.messages.errors_occured'), errors: @new_day_diet.errors.messages}, status: :unprocessable_entity }
      end
    end
  end

  def show
    @dish = Dish.find(params[:id])

    @products = Product.page(params[:page]||1).per(20)
  end

  def destroy
    @dish = Dish.find(params[:id])

    respond_to do |format|
      if @dish.destroy
        format.json { render json: {message: t("global.deleted")}, status: :ok }
      else
        format.json { render json: {message: t('errors.messages.errors_occured')}, status: :unprocessable_entity }
      end
    end
  end

  private

  def dish_params
    params.require(:dish).permit(:name)
  end

end

