class IngredientsController < ApplicationController

  def create
    product = Product.find(params[:ingredient][:product_id])
    @ingredient = Ingredient.new(ingredient_params)
    @dish = Dish.find(params[:ingredient][:dish_id])

    @ingredient.calculate(product,params[:ingredient][:amount])

    respond_to do |format|
      if @ingredient.save
        format.json { render json: {message: t("activerecord.ingredient.ingredient_created"), check: @dish.check_kcal, product: render_to_string(partial: 'products/product_item_onlist', locals: {ingredient: @ingredient}), totals: @dish.display_totals}, status: :ok }
      else
        format.json { render json: {message: t('errors.messages.errors_occured'), errors: @ingredient.errors.messages}, status: :unprocessable_entity }
      end
    end

  end

  def info
    @ingredient = Ingredient.find(params[:id])
    respond_to do |format|
      if @ingredient
        format.json { render json: {info: @ingredient.get_info, id: @ingredient.id}, status: :ok }
      else
        format.json { render json: {message: t('errors.messages.errors_occured')}, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @ingredient = Ingredient.find(params[:id])
    respond_to do |format|
      if @ingredient.destroy
        format.json { render json: {message: "Usunięto", id: @ingredient.id, totals: @ingredient.dish.display_totals}, status: :ok }
      else
        format.json { render json: {message: t('errors.messages.errors_occured')}, status: :unprocessable_entity }
      end
    end
  end

  def update
    @ingredient = Ingredient.find(params[:id])
    product = Product.find(@ingredient.product_id)

    @ingredient.amount=params[:ingredient][:amount]
    @ingredient.calculate(product,params[:ingredient][:amount])

    respond_to do |format|
      if @ingredient.save
        format.json { render json: {message: "Zaaktualizowano", totals: @ingredient.dish.display_totals, check: @ingredient.dish.check_kcal}, status: :ok }
      else
        format.json { render json: {message: t('errors.messages.errors_occured')}, status: :unprocessable_entity }
      end
    end
  end

  private

  def ingredient_params
    params.require(:ingredient).permit(:dish_id, :product_id, :amount)
  end

end

