class HomeController < ApplicationController

  def index
  end

  def info
  end

  def calculators
  end

  def bmi
    weight = params[:weight].to_i
    height = params[:height].to_i/100.to_f.round(2)

    respond_to do |format|
      if weight > 0 and height > 0
        bmi = (weight/(height*height)).to_f.round(2)
        color = ""
        active = ""
        if bmi < 18.5
          color = "lime"
          active = "not"
        elsif (18.5..25).include?(bmi)
          color = ""
          active = "ok"
        elsif bmi > 25
          color = "red"
          active = "bad"
        end
        format.json {render json: {message: "success", bmi: bmi, color: color, active: active}, status: :ok}
      else
        format.json {render json: {message: "error"}, status: :unprocessable_entity}
      end
    end
  end

end

