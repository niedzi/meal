class DayDietsController < ApplicationController

  load_and_authorize_resource

  def new
    @new_day_diet = DayDiet.new
    render layout: false
  end

  def create
    puts params.inspect
    puts day_diet_params.inspect
    diet = Diet.find(params[:diet_id])
    @new_day_diet = diet.day_diets.new(day_diet_params)
    @new_day_diet.user_id = current_user.id
    current_user.day_diets.count == 0 ? redirect = diet_path(diet) : redirect = ""
    respond_to do |format|
      if @new_day_diet.save
        format.json { render json: {message: t("activerecord.day_diet.day_diet_created"), day_diet_item: render_to_string(partial: 'day_diet_item', locals: {day_diet: @new_day_diet}).html_safe, location: redirect}, status: :ok }
      else
        format.json { render json: {message: t('errors.messages.errors_occured'), errors: @new_day_diet.errors.messages}, status: :unprocessable_entity }
      end
    end
  end

  def show
    @day_diet = DayDiet.includes(:diet,:dishes).find(params[:id])
    @dishes = @day_diet.dishes
  end

  def destroy
    @day_diet = DayDiet.find(params[:id])

    puts @day_diet
    respond_to do |format|
      if @day_diet.destroy
        format.json { render json: {message: t("global.deleted")}, status: :ok }
      else
        format.json { render json: {message: t('errors.messages.errors_occured')}, status: :unprocessable_entity }
      end
    end
  end

  private

  def day_diet_params
    params.require(:day_diet).permit(:name)
  end

end

