class DietsController < ApplicationController

  load_and_authorize_resource

  before_action :set_user, :index

  def index
    @diets = @user.diets
  end

  def new
    @new_diet = Diet.new
    render layout: false
  end

  def create
    @new_diet = current_user.diets.new(diet_params)
    current_user.diets.count == 0 ? redirect = diets_path : redirect = ""
    respond_to do |format|
      if @new_diet.save
        format.json { render json: {message: t("activerecord.diet.diet_created"), diet_item: render_to_string(partial: 'diet_item', locals: {diet: @new_diet}).html_safe, location: redirect}, status: :ok }
      else
        format.json { render json: {message: t('errors.messages.errors_occured'), errors: @new_diet.errors.messages}, status: :unprocessable_entity }
      end
    end
  end

  def show
    @diet = Diet.find(params[:id])
    @day_diets = @diet.day_diets
  end

  def destroy
    @diet = Diet.find(params[:id])

    respond_to do |format|
      if @diet.destroy
        format.json { render json: {message: t("global.deleted")}, status: :ok }
      else
        format.json { render json: {message: t('errors.messages.errors_occured')}, status: :unprocessable_entity }
      end
    end
  end

  private

  def diet_params
    params.require(:diet).permit(:name, :kcal)
  end

  def set_user
    @user = current_user
  end

end

