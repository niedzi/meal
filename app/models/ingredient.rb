class Ingredient < ApplicationRecord
  belongs_to :dish
  belongs_to :product

  FIELDS = [:kcal, :fat, :carbohydrates, :protein]
  def calculate(product,amount)
    amount = BigDecimal.new(amount)
    amount /= 100
    self.fat = (product.fat * amount).round(2)
    self.carbohydrates = (product.carbohydrates * amount).round(2)
    self.protein = (product.protein * amount).round(2)
    self.kcal = (product.kcal * amount).round(2)
  end

  def get_info
    str = ""
    FIELDS.each do |x|
      str += "<div class='col s3'><strong>#{I18n.t("activerecord.attributes.products.#{x.to_s}")}</strong><br> #{self.send(x).to_s}</div>"
    end
    str+="<label for='amount'>Ilość:</label>"
    str+="<input id='amount_update' name='ingredient[amount]' value='#{self.amount}'>"
    str.html_safe
  end

end
