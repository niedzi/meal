class Diet < ApplicationRecord
  belongs_to :user
  has_many :day_diets, dependent: :destroy

  validates :name, presence: true
  validates :kcal, presence: true
  validates :kcal, numericality: {greater_than: 0}
end
