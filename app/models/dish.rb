class Dish < ApplicationRecord
  belongs_to :day_diet
  belongs_to :user
  has_many :ingredients, dependent: :destroy

  FIELDS = [:kcal, :fat, :carbohydrates, :protein]

  FIELDS.each do |x|
    define_method "total_#{x}" do
      self.ingredients.sum(x)
    end
  end

  def display_totals
    str = ""
    FIELDS.each do |x|
      if x == :kcal
        str += "<div class='col s3'><strong>#{I18n.t("activerecord.attributes.products.#{x.to_s}")}</strong><br> #{self.check_kcal ? "<strong style='color: red;'>#{self.send("total_#{x}").to_s}</strong> (#{self.day_diet.diet.kcal})" : self.send("total_#{x}").to_s }</div>"
      else
        str += "<div class='col s3'><strong>#{I18n.t("activerecord.attributes.products.#{x.to_s}")}</strong><br> #{self.send("total_#{x}").to_s}</div>"
      end
    end
    str.html_safe
  end

  def check_kcal
    self.day_diet.diet.kcal < self.total_kcal ? true : false
  end

end
