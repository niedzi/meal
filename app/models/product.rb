class Product < ApplicationRecord
  belongs_to :category
  has_many :ingredients

  scope :name_search, -> (name) {where("name LIKE '%#{name}%'")}
  scope :protein_gte, -> (val) {where("protein >= ?",val)}
  scope :fat_gte, -> (val) {where("fat >= ?",val)}
  scope :carbohydrates_gte, -> (val) {where("carbohydrates >= ?",val)}
end
