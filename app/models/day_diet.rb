class DayDiet < ApplicationRecord
  belongs_to :diet
  belongs_to :user
  has_many :dishes, dependent: :destroy
end
