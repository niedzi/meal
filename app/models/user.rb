class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable#, :confirmable

  has_many :diets
  has_many :day_diets
  has_many :dishes

  ROLE = [:admin, :user]

  ROLE.each do |x|
    define_method "#{x}?" do
      self.role.to_s == x.to_s and self.id.present?
    end
  end
end
